package eai.controller

import eai.model.LinksWrapper
import eai.model.ResponseModel
import eai.model.response.ErrorResponse
import eai.model.response.CryptoResponse
import eai.model.response.SymbolsResponse
import eai.service.CryptoTickerService
import eai.service.CurrencyConversionService

import com.beust.klaxon.Parser
import com.beust.klaxon.JsonObject
import java.io.StringReader
import java.math.RoundingMode
import java.text.DecimalFormat
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping("crypto")
@RestController
class CryptoController(
    val cryptoTickerService: CryptoTickerService,
    val currencyConversionService: CurrencyConversionService) {

    @GetMapping("")
    fun getSymbols(request:HttpServletRequest): ResponseModel {
        return SymbolsResponse(
            supportedCryptos = cryptoTickerService.getAvailableSymbols(),
            links = LinksWrapper()
                .add(rel="self", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/"))
                .add(rel="crypto_usd", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/<supported_crypto>"))
                .unwrap())
    }

    @GetMapping("{crypto}")
    fun getCryptoBaseValue(@PathVariable crypto:String,
            request: HttpServletRequest): ResponseModel {
        val value = cryptoTickerService.getCryptoBaseValue(crypto)
        if(value > 0){
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            val formattedValue = df.format(value)
            return CryptoResponse(
                name = crypto.toLowerCase().plus("_usd"),
                description = crypto.toUpperCase().plus(" value in United States Dollar."),
                value = formattedValue,
                links = LinksWrapper()
                    .add(rel="self", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()))
                    .add(rel="supported_currencies", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/"))
                    .add(rel="fiat_value", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/<supported_currency>"))
                    .unwrap())
        } else {
            return ErrorResponse(
                error = "CRYPTO_INVALID_OR_UNSUPPORTED",
                description = crypto.toUpperCase().plus(" is not recognized or supported in Brave New Coin (BNC) API."),
                links = LinksWrapper()
                    .add(rel="self", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()))
                    .add(rel="supported_cryptos", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/"))
                    .unwrap())
        }
    }

}