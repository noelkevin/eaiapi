package eai.controller

import eai.model.LinksWrapper
import eai.model.ResponseModel
import eai.model.response.ErrorResponse
import eai.model.response.FiatResponse
import eai.model.response.CurrenciesResponse
import eai.service.CryptoTickerService
import eai.service.CurrencyConversionService

import com.beust.klaxon.Parser
import com.beust.klaxon.JsonObject
import java.io.StringReader
import java.math.RoundingMode
import java.text.DecimalFormat
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping("crypto/{crypto}/fiat")
@RestController
class FiatController(
    val cryptoTickerService: CryptoTickerService,
    val currencyConversionService: CurrencyConversionService) {

    @GetMapping("")
    fun getCurrencies(@PathVariable crypto:String,
        request:HttpServletRequest): ResponseModel {
        return CurrenciesResponse(
            supportedCurrencies = currencyConversionService.getAvailableCurrencies(),
            links = LinksWrapper()
                .add(rel="self", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/"))
                .add(rel=crypto.toLowerCase().plus("_to_fiat"), href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/").plus("<supported_currency>"))
                .unwrap())
    }

    @GetMapping("{fiat}")
    fun getFiatConversion(@PathVariable crypto:String,
            @PathVariable fiat:String,
            request:HttpServletRequest): ResponseModel {
        val usd = cryptoTickerService.getCryptoBaseValue(crypto)
        if(usd <= 0){
            return ErrorResponse(
                error = "CRYPTO_INVALID_OR_UNSUPPORTED",
                description = crypto.toUpperCase().plus(" is not recognized or supported in Brave New Coin (BNC) API."),
                links = LinksWrapper()
                    .add(rel="self", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/").plus(fiat.toUpperCase()))
                    .add(rel="supported_cryptos", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/"))
                    .unwrap())
        }
        val converted = currencyConversionService.getConvertedValue(amount = usd, from = "USD", to = fiat)

        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING

        if(converted > 0){
            return FiatResponse(
                crypto_name = crypto.toUpperCase(),
                fiat_name = fiat.toUpperCase(),
                fiat_value = df.format(converted),
                links = LinksWrapper()
                    .add(rel="self", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/").plus(fiat.toUpperCase()))
                    .add(rel="base_usd_value", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()))
                    .unwrap())
        } else {
                return ErrorResponse(
                error = "FIAT_INVALID_OR_UNSUPPORTED",
                description = fiat.toUpperCase().plus(" is not recognized or supported in exchangeratesapi.io API (Euro foreign exchange reference rate)."),
                links = LinksWrapper()
                    .add(rel="self", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/").plus(fiat.toUpperCase()))
                    .add(rel="supported_currencies", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/").plus(crypto.toUpperCase()).plus("/fiat/"))
                    .unwrap())
        }
    }
}