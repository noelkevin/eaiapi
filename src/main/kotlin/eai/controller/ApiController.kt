package eai.controller

import eai.model.LinksWrapper
import eai.model.ResponseModel
import eai.model.response.IndexResponse
import eai.service.ApiService

import javax.servlet.http.HttpServletRequest
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping("/")
@RestController
class ApiController(val api: ApiService) {

    @GetMapping("")
    fun index(request:HttpServletRequest): IndexResponse {
        return IndexResponse(
            name = "cryptoconverter",
            version = api.getVersion(),
            description = "Lets you get various cryptocurrency current value in specific currency.",
            links = LinksWrapper()
                .add(rel="root_url", href=request.getHeader("Host").plus(request.getContextPath()))
                .add(rel="supported_cryptos_collection", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/"))
                .add(rel="crypto_usd_value_resource", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/<supported_crypto>"))
                .add(rel="supported_currencies_collection", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/<supported_crypto>/fiat/"))
                .add(rel="crypto_fiat_value_resource", href=request.getHeader("Host").plus(request.getContextPath()).plus("/crypto/<supported_crypto>/fiat/<supported_currency>"))
                .unwrap())
    }

}