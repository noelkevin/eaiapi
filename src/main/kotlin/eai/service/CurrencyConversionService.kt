package eai.service

import eai.service.ConversionDao
import org.springframework.stereotype.Service

@Service
class CurrencyConversionService(val conversionSource: ConversionDao) {

    fun getConvertedValue(amount:Double, from:String, to:String): Double {
        return conversionSource.convert(amount=amount, from=from, to=to)
    }

    fun getAvailableCurrencies(): MutableSet<String> {
        return conversionSource.getCurrencies()
    }

}

