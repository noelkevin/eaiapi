package eai.service

import java.io.StringReader
import com.beust.klaxon.Parser
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@Repository
class TickerDao {

    val parser: Parser = Parser.default()

    fun getTicker(crypto:String): Double {
        val restTemplate: RestTemplate = RestTemplate()

        val headers: HttpHeaders = HttpHeaders()
        headers.add("X-RapidAPI-Host", System.getenv().get("X_RAPIDAPI_HOST_BRAVENEWCOIN"))
        headers.add("X-RapidAPI-Key", System.getenv().get("RAPIDAPI_KEY"))
        val httpEntity: HttpEntity<String> = HttpEntity<String>(headers)

        val builder: UriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(System.getenv().get("BRAVENEWCOIN_TICKER")!!)
            .queryParam("show", "usd")
            .queryParam("coin", crypto)

        val response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, httpEntity, String::class.java)
        val json = parser.parse(StringReader(response.getBody())) as JsonObject

        var usd: String = json.string("last_price") ?: "0.0"
        return usd.toDouble()
    }

    fun getSymbols(): List<String> {
        val restTemplate: RestTemplate = RestTemplate()

        val headers: HttpHeaders = HttpHeaders()
        headers.add("X-RapidAPI-Host", System.getenv().get("X_RAPIDAPI_HOST_BRAVENEWCOIN"))
        headers.add("X-RapidAPI-Key", System.getenv().get("RAPIDAPI_KEY"))
        val httpEntity: HttpEntity<String> = HttpEntity<String>(headers)

        val response = restTemplate.exchange(System.getenv().get("BRAVENEWCOIN_SYMBOLS"), HttpMethod.GET, httpEntity, String::class.java)
        val json = parser.parse(StringReader(response.getBody())) as JsonObject
        val symbolsArray:JsonArray<JsonObject> = json.array("digital_currencies")!!

        return symbolsArray.flatMap{it.keys}
    }

}