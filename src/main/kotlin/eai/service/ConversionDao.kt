package eai.service

import java.io.StringReader
import com.beust.klaxon.Parser
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import org.springframework.util.MultiValueMap
import org.springframework.stereotype.Controller
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@Repository
class ConversionDao {

    val parser: Parser = Parser.default()

    fun convert(amount:Double, from:String, to:String): Double {
        val restTemplate: RestTemplate = RestTemplate()

        val builder: UriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(System.getenv().get("EXCHANGERATES_LATEST")!!)
            .queryParam("base", from)

        val response = restTemplate.getForEntity(builder.toUriString(), String::class.java)
        val json = parser.parse(StringReader(response.getBody())) as JsonObject
        var rate = json.obj("rates")?.double(to.toUpperCase()) ?: 0.0
        val result:Double = rate * amount

        return result
    }

    fun getCurrencies(): MutableSet<String> {
        val restTemplate: RestTemplate = RestTemplate()

        val response = restTemplate.getForEntity(System.getenv().get("EXCHANGERATES_LATEST"), String::class.java)
        val json = parser.parse(StringReader(response.getBody())) as JsonObject
        var currencies = json.obj("rates")!!.map.keys

        return currencies
    }

}

