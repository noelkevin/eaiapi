package eai.service

import eai.service.TickerDao

import org.springframework.stereotype.Service

@Service
class CryptoTickerService(val tickerSource: TickerDao) {

    fun getCryptoBaseValue(crypto:String): Double {
        return tickerSource.getTicker(crypto)
    }

    fun getAvailableSymbols(): List<String>{
        return tickerSource.getSymbols()
    }

}