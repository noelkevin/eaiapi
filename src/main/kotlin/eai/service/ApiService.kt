package eai.service

import org.springframework.stereotype.Service

@Service
class ApiService {

    fun getVersion(): String {
        return System.getenv().get("API_VERSION_PATH") ?: "err"
    }

}
