package eai.model

import eai.model.LinkModel

interface ResponseModel{
    val links: MutableList<LinkModel>
}
