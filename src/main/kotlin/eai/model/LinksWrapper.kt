package eai.model

import eai.model.LinkModel

class LinksWrapper(){
    val links: MutableList<LinkModel> = ArrayList<LinkModel>()

    fun add(rel:String, href:String):LinksWrapper{
        this.links.add(LinkModel(rel=rel, href=href))
        return this
    }

    fun unwrap():MutableList<LinkModel>{
        return this.links
    }

}