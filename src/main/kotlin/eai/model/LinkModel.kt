package eai.model

data class LinkModel(val rel: String, val href: String)