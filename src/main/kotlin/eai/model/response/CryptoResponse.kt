package eai.model.response

import eai.model.LinkModel
import eai.model.ResponseModel

data class CryptoResponse(
    val name: String,
    val description: String,
    val value: String,
    override val links: MutableList<LinkModel>
): ResponseModel



