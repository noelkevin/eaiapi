package eai.model.response

import eai.model.LinkModel
import eai.model.ResponseModel

import com.beust.klaxon.JsonObject

data class CurrenciesResponse(
    val supportedCurrencies: MutableSet<String>,
    override val links: MutableList<LinkModel>
): ResponseModel


