package eai.model.response

import eai.model.LinkModel
import eai.model.ResponseModel

import com.beust.klaxon.JsonObject

data class IndexResponse(
    val name: String,
    val version: String,
    val description: String,
    override val links: MutableList<LinkModel>
): ResponseModel


