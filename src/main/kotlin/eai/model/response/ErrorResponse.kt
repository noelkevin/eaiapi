package eai.model.response

import eai.model.LinkModel
import eai.model.ResponseModel

data class ErrorResponse(
    val error: String,
    val description: String,
    override val links: MutableList<LinkModel>
): ResponseModel



