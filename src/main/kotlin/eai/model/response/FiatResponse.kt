package eai.model.response

import eai.model.LinkModel
import eai.model.ResponseModel

data class FiatResponse(
    val crypto_name: String,
    val fiat_name: String,
    val fiat_value: String,
    override val links: MutableList<LinkModel>
): ResponseModel



