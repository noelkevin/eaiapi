package eai.model.response

import eai.model.LinkModel
import eai.model.ResponseModel

import com.beust.klaxon.JsonObject

data class SymbolsResponse(
    val supportedCryptos: List<String>,
    override val links: MutableList<LinkModel>
): ResponseModel


